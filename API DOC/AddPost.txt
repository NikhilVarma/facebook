- API Title:

    Add Post
 
- API Endpoint:
    
    http://localhost:8080/add-post

    Path After Adding Params:
	NULL

- Request Method:
	
	POST
	
- Request Body:

    NULL
	
- Request Params:

	post : photo.jpeg,
    caption : "cap",
    "postUserMobileNumber":"7027172621",
    "time":"13:02:56",              # HH:MM:SS
    "date":"2001-02-10"             # YYYY-MM-DD


- Path Variables:

	NULL

- Success response:

	 message:success

- Error response:

    "message", "only jpeg data is valid"
	"message", "something went wrong file not uploaded"
