- API Title:

    Bookmark Post
 
- API Endpoint:

    http://localhost:8080/bookmark-post

- Request Method:
	
	POST
	
- Request Body:

    {
        "bookmarkedPostId":"2",
        "bookmarkedByUserMobileNumber":"7028"
    }
	
- Request Params:

	NULL

- Path Variables:

    NULL

- Success response:

    {
        "message" : "success"
    }

- Error response:

	 NULL
